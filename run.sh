#!/bin/sh -x

. common.sh

gcloud dataflow flex-template run "pco-data-loader-`date +%Y%m%d-%H%M%S`" \
    --template-file-gcs-location "$TEMPLATE_PATH" \
    --parameters input_file=gs://pcb-poc-datalake1-bucket-danielz/input/pco_data/Sample_20210917.csv \
    --parameters output_table=$PROJECT:$DATASET.$TABLE \
    --region $REGION \
    --subnetwork regions/northamerica-northeast1/subnetworks/poc-subnet-a \
    --disable-public-ips