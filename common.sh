#!/bin/sh

export PROJECT=pcb-poc-datalake1
export DATASET=pco_dataset
export TABLE=pco_sample
export REGION=northamerica-northeast1
export TEMPLATE_PATH=gs://pcb-poc-datalake1-bucket-danielz/dataflow-templates/pco-data-loader.json
export TEMPLATE_IMAGE="gcr.io/$PROJECT/dataflow/pco-data-loader:latest"
export IGNORE_FILE=.dockerignore