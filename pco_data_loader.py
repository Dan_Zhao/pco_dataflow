import argparse
import logging
import re
import os
import yaml
import uuid
import apache_beam as beam

from datetime import datetime
from apache_beam.io.gcp.bigquery import WriteToBigQuery, BigQueryDisposition
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions


def load_schema():
    schema_file_path = '/template/pco_schema.yaml'

    if os.path.exists(schema_file_path):
        with open(schema_file_path, 'r') as schema_file:
            return yaml.safe_load(schema_file)


def extract_columns():
    if not SCHEMA:
        logging.error('Schema is not available')
        return

    fields = SCHEMA.get('fields')
    columns = [d['name'] for d in fields]

    return columns


def build_row(line):
    logging.info(f'line={line}')
    values = re.split("\\|", re.sub('\r\n', '', re.sub('"', '', line)))+[RUN_ID, LOAD_DATE]
    row = dict(zip(COLUMNS, values))

    logging.info(f'row={row}')

    return row


def run(argv=None, save_main_session=True):
    """Main entry point; defines and runs the wordcount pipeline."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_file',
        dest='input_file',
        required=True,
        help='Location of the input PCO file.')
    parser.add_argument(
        '--output_table',
        dest='output_table',
        required=True,
        help='Name of the BigQuery output table name.')

    known_args, pipeline_args = parser.parse_known_args(argv)

    # We use the save_main_session option because one or more DoFn's in this
    # workflow rely on global context (e.g., a module imported at module level).
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = save_main_session

    logging.info(f"pipeline_options: {pipeline_options.__dict__}")

    # The pipeline will be run on exiting the with block.
    with beam.Pipeline(options=pipeline_options) as p:
        _ = (
                p
                | 'Read csv file' >> beam.io.ReadFromText(known_args.input_file, skip_header_lines=1)
                | 'Convert to BigQuery rows' >> beam.Map(build_row)
                | 'Load rows into BigQuery' >> WriteToBigQuery(
                    known_args.output_table,
                    schema=pco_schema,
                    write_disposition=BigQueryDisposition.WRITE_TRUNCATE,
                    create_disposition=BigQueryDisposition.CREATE_IF_NEEDED)
        )

        p.run().wait_until_finish()


SCHEMA = load_schema()
COLUMNS = extract_columns()
RUN_ID = str(uuid.uuid4())
LOAD_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S%z')

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    run()
