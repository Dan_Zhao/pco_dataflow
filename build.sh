#!/bin/sh -x
. common.sh

gcloud config set builds/use_kaniko True
gcloud config set project $PROJECT

if gcloud builds submit --no-cache --ignore-file=$IGNORE_FILE --tag=$TEMPLATE_IMAGE .
  then
    gcloud dataflow flex-template build $TEMPLATE_PATH \
          --image $TEMPLATE_IMAGE \
          --sdk-language "PYTHON" \
          --metadata-file "metadata.json"
else
  echo "Failed in building Docker image"
fi